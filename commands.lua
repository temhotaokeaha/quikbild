local S = minetest.get_translator("quikbild")

ChatCmdBuilder.new("quikbild", function(cmd)

  -- create arena
  cmd:sub("create :arena", function(name, arena_name)
      arena_lib.create_arena(name, "quikbild", arena_name)
  end)

  cmd:sub("create :arena :minplayers:int :maxplayers:int", function(name, arena_name, min_players, max_players)
      arena_lib.create_arena(name, "quikbild", arena_name, min_players, max_players)
  end)

  -- remove arena
  cmd:sub("remove :arena", function(name, arena_name)
      arena_lib.remove_arena(name, "quikbild", arena_name)
  end)

  -- list of the arenas
  cmd:sub("list", function(name)
      arena_lib.print_arenas(name, "quikbild")
  end)

  -- enter editor mode
  cmd:sub("edit :arena", function(sender, arena)
      arena_lib.enter_editor(sender, "quikbild", arena)
  end)

  -- enable and disable arenas
  cmd:sub("enable :arena", function(name, arena)
      arena_lib.enable_arena(name, "quikbild", arena)
  end)

  cmd:sub("disable :arena", function(name, arena)
      arena_lib.disable_arena(name, "quikbild", arena)
  end)

  cmd:sub("version", function(name)
    minetest.chat_send_player(name,S("The version of QuikBild is").." "..quikbild.version)
  end)


end, {
  description = [[
    (/help quikbild)
    Use this to configure your arena:
    - create <arena name> [min players] [max players]
    - edit <arena name>
    - enable <arena name>
    Other commands:
    - remove <arena name>
    - disable <arena>
    ]],
  privs = { quikbild_admin = true }
})



minetest.register_chatcommand("qblang", {
  params = "",
  description = "Set Quickbild language: use /qblang <code>  Available codes: en,it,es,fr,de",
  func = function(name, param)
      quikbild.send_lang_fs(name)
  end,
})



minetest.register_chatcommand("qblang_send", {
  params = "",
  privs = { quikbild_admin = true },
  description = "Send a player the language selector formspec",
  func = function(name, param)
      if minetest.get_player_by_name(param) then
        quikbild.send_lang_fs(param)
      end
  end,
})

minetest.register_on_mods_loaded( function()
  local old_me_func = minetest.registered_chatcommands["me"].func

  minetest.override_chatcommand("me", {
    func = function(name,param)
      local mod = arena_lib.get_mod_by_player(name)
      if mod == "quikbild" then
        local arena = arena_lib.get_arena_by_player(name)
        if arena then
          if arena.artist and arena.artist == name then
            return true, minetest.colorize("#eea160","[!] /me "..S("is disabled while being the artist in quikbild. Don't cheat!"))
          end
        end
      end
      old_me_func(name,param)
    end
  })


  local old_tell_func = minetest.registered_chatcommands["tell"].func

  minetest.override_chatcommand("tell", {
    func = function(name,param)
      local mod = arena_lib.get_mod_by_player(name)
      if mod == "quikbild" then
        local arena = arena_lib.get_arena_by_player(name)
        if arena then
          if arena.artist and arena.artist == name then
            return true, minetest.colorize("#eea160","[!] /tell "..S("is disabled while being the artist in quikbild. Don't cheat!"))
          end
        end
      end
      old_tell_func(name,param)
    end
  })


  local old_msg_func = minetest.registered_chatcommands["msg"].func
  minetest.override_chatcommand("msg", {
    func = function(name,param)
      local mod = arena_lib.get_mod_by_player(name)
      if mod == "quikbild" then
        local arena = arena_lib.get_arena_by_player(name)
        if arena then
          if arena.artist and arena.artist == name then
            return true, minetest.colorize("#eea160","[!] /msg "..S("is disabled while being the artist in quikbild. Don't cheat!"))
          end
        end
      end
      old_msg_func(name,param)
    end
  })

end)