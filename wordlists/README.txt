the columns for the wordlists are translations as follows:

english | italian | spanish | french | german


Every word in the wordlist MUST be translated in all supported languages. Supporting another language would involve translating all the words.
