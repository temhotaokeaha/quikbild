local S = minetest.get_translator("quikbild")

minetest.register_privilege("quikbild_admin", {
    description = S("With this you can use /quikbild create <arena_name>,/quikbild edit <arena_name>")
})
